package com.glearning.maven.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.glearning.maven.client.Calculator;

public class HelloWorldTest {
	
	@Test
	public void testSum() {
		Calculator calculator = new Calculator();
		int result = calculator.sum(45, 30);
		Assertions.assertEquals(75, result);
	}
	
	@Test
	public void testMultiply() {
		Calculator calculator = new Calculator();
		int result = calculator.multiply(45, 30);
		Assertions.assertEquals(1350, result);
	}

}
